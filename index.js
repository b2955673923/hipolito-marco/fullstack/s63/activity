function countLetter(letter, sentence) {
    let result = 0;

    
    if (letter.length !== 1) {
        return undefined;
    } 

    let count = 0;

    for(let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
            count++;
        }
    }
    return count;
}


function isIsogram(text) {
    
    const lowercaseText = text.toLowerCase();
    const encounteredLetters = [];

    for (let i = 0; i < lowercaseText.length; i++) {
      const currentChar = lowercaseText[i];

      if (encounteredLetters.indexOf(currentChar) !== -1) {
        return false;
      }

      encounteredLetters.push(currentChar);
    }

    return true;
}


function purchase(age, price) {
  
  if (age < 13) {
    return undefined; 
  } 
  else if (age >= 13 && age <= 21 || age >= 65) {
    const discountedPrice = (0.8 * price).toFixed(2); 
    return discountedPrice.toString();
  } 
  else if (age >= 22 && age <= 64) {
    const roundedPrice = price.toFixed(2); 
    return roundedPrice.toString();
  } 
  else {
    return undefined; 
  }
}

function findHotCategories(items) {
  const hotCategories = new Set();

  for (let i = 0; i < items.length; i++) {
      if (items[i].stocks === 0) {
        hotCategories.add(items[i].category);
      }
  }

  return Array.from(hotCategories); // Convert the Set to an array and return it.
}


const items = [
  { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
  { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
  { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
  { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
  { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
];

const hotCategories = findHotCategories(items);



function findFlyingVoters(candidateA, candidateB) {
    

    const flyingVoters = candidateA.filter((voter) => candidateB.includes(voter));
      return flyingVoters;
    }

    const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    const flyingVoters = findFlyingVoters(candidateA, candidateB);


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};


